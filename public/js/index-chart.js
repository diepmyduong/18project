$('document').ready(() => {

    var defaultMapOption = {
        backgroundColor: 'white',
        animation: true,
        animationDuration: 1000,
        animationEasing: 'cubicInOut',
        animationDurationUpdate: 1000,
        animationEasingUpdate: 'cubicInOut',
        title: {
            // text: 'Word Map',
            // subtext: 'Sub Text',
            left: 'center',
            textStyle: { color: 'black' }
        },
        tooltip: { trigger: 'item' },
        visualMap: {
            min: 0,
            max: 200,
            calculable: true,
            inRange: { color: ['lightskyblue', 'yellow', 'orangered'] },
            textStyle: { color: 'black' },
        },
        geo: {
            map: 'world',
            label: {
                emphasis: { show: true, textStyle: { color: 'black' } },
                normal: { show: false }
            },
            itemStyle: {
                normal: { areaColor: '#bbb', borderColor: '#111' },
                emphasis: { areaColor: '#ddd' }
            },
            zoom: 2,
            roam: true,
            left: 300,
            center: [105.7923584,19.35771561]
        },
        grid: {
            right: 40,
            top: 100,
            bottom: 40,
            width: '30%'
        },
        xAxis: {
            type: 'value',
            scale: true,
            position: 'top',
            boundaryGap: false,
            splitLine: {show: false},
            axisLine: {show: false},
            axisTick: {show: false},
            axisLabel: {margin: 2, textStyle: {color: '#aaa'}},
        },
        yAxis: {
            type: 'category',
            name: 'TOP 10',
            nameGap: 16,
            axisLine: {show: false, lineStyle: {color: 'black'}},
            axisTick: {show: false, lineStyle: {color: 'black'}},
            axisLabel: {interval: 0, textStyle: {color: 'black'}},
            data: []
        },
    }
    // Global Config
    var wordldMapOption = _.merge({}, defaultMapOption);
    var vnMapOption = _.merge({}, defaultMapOption);
    _.set(vnMapOption, 'geo.map', 'vnm');

    // Get Map Geo Json
    function getMap(name, lv = 'lv_1') {
        return new Promise((resolve, reject) => {
            $.get(`json/map/${name}/${lv}.json`, function (data, status) {
                if (status == 'success') resolve(data);
                else {
                    console.log('cannot get map geo for ', name);
                    reject();
                }
            })
        })
    }
    // Convert Data
    function convertData(data, geoJSON) {
        var res = [];
        for (var i = 0; i < data.length; i++) {
            var geoCoord = geoCoordMap[data[i].name];
            if (geoCoord) {
                res.push(geoCoord.concat(data[i].value));
            }
        }
        return res;
    };
    // Set Map Option
    function setMapOption(elementId, option) {
        var chart = echarts.init(document.getElementById(elementId));
        chart.setOption(option);
    }
    // Generate Data
    function generateData(keys, option = { take: 10, min: 0, max: 100, float: false }) {
        const shuffledKeys = _.take(_.shuffle(keys), option.take);
        const result = [];
        for (var k of shuffledKeys) {
            result.push({
                name: k,
                value: _.random(option.min, option.max, option.float)
            });
        }
        return result
    }

    async function run() {
        // Get All Geo JSON
        const [wordMapGeoJson, vnMapGeoJson] = await Promise.all([getMap('world'), getMap('vnm')]);
        // Reformat VNMapGeoJSON features
        vnMapGeoJson.features = vnMapGeoJson.features.map(g => {
            return {
                type: 'Feature',
                id: g.properties.HASC_1,
                properties: { name: g.properties.NAME_1 },
                geometry: g.geometry
            }
        })
        // Regis Map
        // echarts.registerMap('world', wordMapGeoJson);
        echarts.registerMap('vnm', vnMapGeoJson);
        // Generate Random Data
        const worldData = generateData(wordMapGeoJson.features.map(f => f.properties.name), { take: 60, min: 5000, max: 20000 });
        const vnData = generateData(vnMapGeoJson.features.map(f => f.properties.name), { take: 50, min: 10, max: 500 });
        console.log('worldData', worldData)
        // Config Map Option
        _.set(wordldMapOption, 'visualMap.min', 5000);
        _.set(wordldMapOption, 'visualMap.max', 20000);
        _.set(vnMapOption, 'visualMap.min', 10);
        _.set(vnMapOption, 'visualMap.max', 500);
        // Add Series
        _.set(wordldMapOption, 'series', [{ name: 'pm2.5', type: 'map', geoIndex: 0, data: worldData }]);
        _.set(vnMapOption, 'series[0]', { name: 'pm2.5', type: 'map', geoIndex: 0, data: vnData });
        // Set Top 10
        const top10 = _.take(_.reverse(_.sortBy(vnData,'value')),10);
        _.set(vnMapOption, 'yAxis.data', _.map(top10, 'name'));
        _.set(vnMapOption, 'series[1]', { id: 'bar', zlevel: 2, type: 'bar', symbol: 'none',
            itemStyle: {
                normal: {
                    color: '#ddb926'
                }
            },
            data: _.reverse(top10)
        })

        // Set Option
        // setMapOption('world-chart', wordldMapOption);
        setMapOption('vn-chart', vnMapOption);
    }

    run();
})
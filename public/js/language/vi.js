jsI18n.addLocale("vi", {
    "support_us": "Liên hệ với chúng tôi :",
    "call_now": "Gọi ngay",
    "donate_now": "Quyên góp ngay",
    "make_a_donation": "Tôi muốn quyên góp",
    "donation_amount": "Số tiền quyên góp",
    "or": "Hoặc",
    "billing_info": "Thông tin tài khoản",
    "card_number": "Số thẻ ",
    "cvc": "Mã bảo mật (CVC) ",
    "name": "Tên ",
    "phone": "Điện thoại ",
    "address": "Địa chỉ ",
    "city": "Thành phố",
    "payment_info": "Thông tin chuyển khoản",
    "home": "Trang chủ",
    "about": "Về chúng tôi",
    "events": "Sự kiện",
    "volunteer": "Tình nguyện viên",
    "contact": "Liên hệ",
    "learn_more": "Xem thêm",
    "donation": "Quyên góp",
    "join_now": "Tham gia ngay",
    "fundraise": "Gây quỹ",
    "read_more": "Xem thêm",
    "upcoming_event": "Sự Kiện Sắp Tới",
    "featured_causes": "Khóa học tiêu biểu",
    "raised": "Đạt được",
    "goal": "Mục tiêu",
    "help_us_by_share": "Chung tay bằng cách chia sẻ:",
    "recent_causes": "Recent Causes",
    "sponsor_a_child_today": "Tài trợ cho một đứa trẻ",
    "donators": "Người quyên góp",
    "our_mission": "Sứ mệnh của chúng tôi",
    "be_a_part_of_us": "Hãy trở thành một phần của chúng tôi",
    "total_awards": "Total Awards",
    "total_volunteer": "Số Tình Nguyện Viên",
    "total_projects": "Số Dự Án",
    "meet_our_volunteers": "Gặp mặt tình nguyện viên",
    "view_profile": "Xem hồ sơ",
    "news": "Tin tức",
    "latest_news": "Tin gần đây",
    "contact_form": "Để lại liên hệ",
    "send": "Gửi",
    "full_name": "Họ và tên",
    "email_address": "Email",
    "your_message": "Nội dung",
    "search": "Tìm kiếm...",
    "statistic": "Thống kê",
})